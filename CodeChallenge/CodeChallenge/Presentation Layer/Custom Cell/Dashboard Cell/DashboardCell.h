//
//  DashboardCell.h
//  CodeChallenge
//
//  Created by Ourangzaib khan on 6/30/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "AksStraightPieChart.h"
@interface DashboardCell : BaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *durationTime;
@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UILabel *minValue;
@property (weak, nonatomic) IBOutlet UILabel *maxValue;
@property (weak, nonatomic) IBOutlet UILabel *labelValue;
@property (weak, nonatomic) IBOutlet AksStraightPieChart *pieChart;
@property (weak, nonatomic) IBOutlet UILabel *averageValue;

@end
