//
//  BaseTableViewCell.m
//  BusService
//
//  Created by Ourangzaib khan on 6/5/15.
//  Copyright (c) 2015 Ourangzaib khan. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
