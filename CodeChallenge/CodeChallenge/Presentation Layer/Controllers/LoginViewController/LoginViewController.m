//
//  LoginViewController.m
//  CodeChallenge
//
//  Created by Ourangzaib khan on 6/30/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import "LoginViewController.h"
#import "Network.h"
#import "DashBoardViewController.h"
@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *passWord;
@property (strong, nonatomic) NSDictionary *dictionary;
@end

@implementation LoginViewController
NSString *name = @"";
NSString *password = @"";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated{
    self.title = @"";
}
-(void) viewWillAppear:(BOOL)animated{
    self.title = @"codding challenge";
}

-(void)verifyUser{
    
    if(password != nil  ){
        password = _passWord.text;
    }
    if(_userName.text != nil){
        name = _userName.text;
    }
    
}

-(void)showMessage:(NSString*)message withTitle:(NSString *)title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController animated:YES completion:^{
        }];
    });
}

#pragma mark - buttons actions

- (IBAction)Signin:(id)sender {
    
    [self verifyUser];
    
    Network *manager= [Network sharedInstance];
    [manager SigninUser:name Password:password completion:^(NSDictionary *dictionaryJson){
        if(dictionaryJson != nil){
            self.dictionary = dictionaryJson;
            [self performSegueWithIdentifier:@"Dashboard" sender:nil];
        }else{
            
            [self showMessage:@"Username or password is worng" withTitle:@"Failed"];
        }
    }];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"Dashboard"]){
        
        DashBoardViewController *controller = (DashBoardViewController *)segue.destinationViewController;
        controller.dictionary = self.dictionary;
    }
    
    
}


@end
