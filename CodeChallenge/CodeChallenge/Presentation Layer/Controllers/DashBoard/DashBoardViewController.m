//
//  DashBoardViewController.m
//  CodeChallenge
//
//  Created by Ourangzaib khan on 6/30/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import "DashBoardViewController.h"
#import "DashboardCell.h"
#import "Network.h"
#import "Constants.h"
@interface DashBoardViewController ()


@end

@implementation DashBoardViewController

int noOfRows = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableViewdashboard.allowsMultipleSelectionDuringEditing = NO;
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    self.title = @"Dashboard";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    noOfRows = 1;
    [self.tableViewdashboard reloadData];
    
}

#pragma mark - Table View Data Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}


#pragma mark -Table View Data Sources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return noOfRows;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    
    DashboardCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[DashboardCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //setting top label value Actually there was a single object otherwise i would have used the JSon Object mapper
    
    cell.labelValue.text = [self.dictionary valueForKey:@"label"][0];
    
    //Minimum value
    cell.minValue.text = [NSString stringWithFormat:@"%@",[[[[self.dictionary valueForKey:@"surroundingPeriodData"] valueForKey:@"minValue"] valueForKey:@"amountInAggregationCurrency"] valueForKey:@"value"][0]];
    //maximum value
    cell.maxValue.text = [NSString stringWithFormat:@"%@",[[[[self.dictionary valueForKey:@"surroundingPeriodData"] valueForKey:@"maxValue"] valueForKey:@"amountInAggregationCurrency"] valueForKey:@"value"][0]];
    
    //setting Average value
    
    cell.averageValue.text = [NSString stringWithFormat:@"%@",[[[[self.dictionary valueForKey:@"surroundingPeriodData"] valueForKey:@"avgValue"] valueForKey:@"amountInAggregationCurrency"] valueForKey:@"value"][0]];
    cell.value.text = [NSString stringWithFormat:@"%@",[[[self.dictionary valueForKey:@"kpiValue"] valueForKey:@"amountInAggregationCurrency"] valueForKey:@"value"][0]];
    
    
    //Calculating the bar distance for chart
    int maxvalue = [cell.value.text intValue];
    int minimum = [cell.minValue.text intValue];
    int percentage = (minimum/maxvalue) * 100;
    //as the values of minimum is too small so i added 25+ to show the view
    [cell.pieChart addDataToRepresent:percentage + 15 WithColor:golden_star];
    [cell.pieChart addDataToRepresent:100 - percentage + 15 WithColor:PURPLE_COLOR];
    
    
    return cell;
}









#pragma mark -editing and deleting tableview Row
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        noOfRows = noOfRows - 1;
        [self.tableViewdashboard reloadData];
    }
}

@end
