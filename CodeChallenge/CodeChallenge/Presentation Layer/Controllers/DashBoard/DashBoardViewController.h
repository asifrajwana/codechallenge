    //
    //  DashBoardViewController.h
    //  CodeChallenge
    //
    //  Created by Ourangzaib khan on 6/30/16.
    //  Copyright © 2016 Ourangzaib khan. All rights reserved.
    //

    #import "BaseViewController.h"

    @interface DashBoardViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
    @property (weak, nonatomic) IBOutlet UITableView *tableViewdashboard;
    @property (strong, nonatomic) NSDictionary *dictionary;
    @end
