//
//  main.m
//  CodeChallenge
//
//  Created by Ourangzaib khan on 6/30/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
