//
//  Constants.h
//  CodeChallenge
//
//  Created by Ourangzaib khan on 7/1/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark -Colors to be used
#define RGB(r, g, b) [UIColor colorWithRed:(float)r / 255.0 green:(float)g / 255.0 blue:(float)b / 255.0 alpha:1.0]
#define golden_star RGB(0,169,0)
#define PURPLE_COLOR RGB(166,78,252)

@interface Constants : NSObject

@end
