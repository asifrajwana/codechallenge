//
//  Singleton.h
//  BusService
//
//  Created by Ourangzaib khan on 6/8/15.
//  Copyright (c) 2015 Ourangzaib khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Network : NSObject


#pragma mark sharedinstace method
+ (Network*)sharedInstance;


#pragma mark source and destination cities method
-(void)SigninUser:(NSString *) username  Password: (NSString *)password  completion:(void(^)(NSDictionary *dictionary))callback;

@end
