//
//  Singleton.m
//  BusService
//
//  Created by Ourangzaib khan on 6/8/15.
//  Copyright (c) 2015 Ourangzaib khan. All rights reserved.
//

#import "Network.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"



@implementation Network


#pragma mark sharedinstace method
- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}
#pragma mark -Singleton object

+ (Network*)sharedInstance{
    
    static Network *_sharedInstance=nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate,^{
        
        _sharedInstance=[[Network alloc] init];
        
    });
    return _sharedInstance;
}

#pragma mark - source and destination cities method
-(void)SigninUser:(NSString *) username  Password: (NSString *)password completion:(void(^)(NSDictionary *dictionary ))callback{
    NSURL *baseURL = [NSURL URLWithString:@"https://opx.cfapps.io/dashboarditems"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:username password:password];
    
    [manager GET:[baseURL absoluteString]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
             NSLog(@"%@",[json valueForKey:@"code"]);
             callback(json);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             // handle failure
             callback(nil);
             NSLog(@"%@,  %@", [error localizedDescription], [operation responseString]);
         }];
    
}

@end
